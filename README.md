# Dlms Python Poc

# Setup once (Python 3.9)

```
pip install virtualenv
python -m virtualenv venv
```

# Setup everytime

```
.\venv\Scripts\activate
pip install -r requirements.txt
```

# Start

```
python src/main.py
```

# Test

```
python tests/test.py
```

# List available COM-ports

```
python -m serial.tools.list_ports
```

# Add dependency

```
pip install <somepackage>
pip freeze > requirements.txt
```
