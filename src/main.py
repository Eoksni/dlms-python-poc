#!/usr/bin/env python

from dlms_cosem.clients.dlms_client import DlmsClient
from dlms_cosem import enumerations, cosem
from dpp.hdlc_transport import SerialEchoHdlcTransport

from dpp.log import debug
import dpp.config as config

debug("starting up")
transport = SerialEchoHdlcTransport(
    server_logical_address=config.server_logical_address,
    server_physical_address=config.server_physical_address,
    client_logical_address=config.client_logical_address,
    serial_port=config.port,
)
dlmsclient = DlmsClient(
    client_logical_address=config.client_logical_address,
    server_logical_address=config.server_logical_address,
    authentication_method=None,
    password=None,
    encryption_key=None,
    authentication_key=None,
    security_suite=0,
    dedicated_ciphering=False,
    block_transfer=False,
    max_pdu_size=65535,
    client_system_title=None,
    client_initial_invocation_counter=0,
    meter_initial_invocation_counter=0,
    io_interface=transport,
)

with dlmsclient.session() as client:
    debug('connected')

    # data = client.get(
    #     cosem.CosemAttribute(
    #         interface=15,
    #         instance=cosem.Obis(0x0, 0x0, 40, 0x0, 0x0),
    #         attribute=2,
    #     )
    # )  # works! but no idea what that data is

    # data = client.get(
    #     cosem.CosemAttribute(
    #         interface=17,
    #         instance=cosem.Obis(0x0, 0x0, 41, 0x0, 0x0),
    #         attribute=2,
    #     )
    # ) # sap assignment list (list of all logical devices)

    # data = client.get(
    #     cosem.CosemAttribute(
    #         interface=8,
    #         instance=cosem.Obis(0, 0, 1, 0, 0),
    #         attribute=2,
    #     )
    # )  # clock works!

    data = client.get(
        cosem.CosemAttribute(
            interface=1,
            instance=cosem.Obis(0, 0, 96, 1, 0),
            attribute=2,
        )
    )  # serial number
    print(f'Serial number: {data}')


# 7E_A0_1A_02_03_21_32_CD_71_E6_E6_00_C0_01_C1_00_01_00_00_01_01_00_FF_02_00_13_23_7E
# 7E_A0_1A_02_03_21_32_CD_71_E6_E6_00_C0_01_C1_00_01_00_00_01_02_00_FF_02_00_DF_3E_7E
# 7E_A0_1A_02_03_21_32_CD_71_E6_E6_00_C0_01_C1_00_01_00_00_01_00_00_FF_02_00_57_28_7E
    # 7E_A0_1A_02_03_21_32_CD_71_E6_E6_00_C0_01_C1_00_08_00_00_01_00_00_FF_02_00_60_1A_7E
    # Q Tx: 7E_A0_1A_02_03_21_54_FD_77_E6_E6_00_C0_01_C1_00_08_00_00_01_00_00_FF_02_00_60_1A_7E
    # Q Rx: 7E_A0_1F_21_02_03_74_6A_90_E6_E7_00_C4_01_C1_00_09_0C_07_E4_0C_0B_05_02_0A_12_FF_80_00_00_04_73_7E

    # data = client.get(
    #     cosem.CosemAttribute(
    #         interface=1,
    #         instance=cosem.Obis(0x0, 0x0, 0x1, 0x1, 0x0),
    #         attribute=2,
    #     )
    # )

    # print(data)

    debug('done')
