#!/usr/bin/env python

import serial

from dlms_cosem.clients.hdlc_transport import SerialHdlcTransport
from dlms_cosem.protocol.acse import ApplicationAssociationRequestApdu, UserInformation
from dlms_cosem.protocol.xdlms.conformance import Conformance
from dlms_cosem.protocol.xdlms.initiate_request import InitiateRequestApdu

from dpp.hdlc_transport import SerialEchoHdlcTransport
from dpp.log import debug
import dpp.config as config

debug("starting up direct hdlc connection")
client = SerialEchoHdlcTransport(
    server_logical_address=config.server_logical_address,
    server_physical_address=config.server_physical_address,
    client_logical_address=config.client_logical_address,
    serial_port=config.port,
)
with client as c:
    debug('connected')

    # aarq = ApplicationAssociationRequestApdu(
    #     user_information=UserInformation(
    #         content=InitiateRequestApdu(
    #             proposed_conformance=Conformance(
    #                 general_protection=False,
    #                 general_block_transfer=False,
    #                 delta_value_encoding=False,
    #                 attribute_0_supported_with_set=False,
    #                 priority_management_supported=False,
    #                 attribute_0_supported_with_get=False,
    #                 block_transfer_with_get_or_read=True,
    #                 block_transfer_with_set_or_write=True,
    #                 block_transfer_with_action=True,
    #                 multiple_references=True,
    #                 data_notification=False,
    #                 access=False,
    #                 get=True,
    #                 set=True,
    #                 selective_access=True,
    #                 event_notification=False,
    #                 action=True,
    #             ),
    #             client_max_receive_pdu_size=65535,
    #             proposed_quality_of_service=0,
    #             proposed_dlms_version_number=6,
    #             response_allowed=True,
    #             dedicated_key=None,
    #         )
    #     )
    # )

    # debug('created aarq')
    # debug(aarq.to_bytes())
    # response = client.send(aarq.to_bytes())
    # debug('sent aarq')

    # print(response.hex())
