import serial
import dpp.utils as utils

with serial.Serial('COM5', timeout=5, parity=serial.PARITY_EVEN) as ser:
    print(ser.name)         # check which port was really used
    ser.write(utils.convert_hex_string('7E_A0_08_02_03_21_93_86_67_7E'))
    s = ser.read(64)
    print('received')
    print(len(s))
    print(s)
    hexed = s.hex()
    print(len(hexed))
    print(utils.prettify(hexed))
