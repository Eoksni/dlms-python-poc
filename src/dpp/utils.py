def convert_hex_string(hex_str):
    return bytes.fromhex(hex_str.replace('_', '').replace(' ', ''))


def prettify(my_str, group=2, char='_'):
    my_str = bytes(my_str).hex()
    my_str = my_str.upper()
    return char.join(my_str[i:i+group] for i in range(0, len(my_str), group))
