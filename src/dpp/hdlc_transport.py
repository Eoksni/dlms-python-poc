from typing import Optional
import attr
import serial
from dlms_cosem.clients.hdlc_transport import SerialHdlcTransport
from dlms_cosem.hdlc import address, connection, frames, state

from dpp.log import debug
import dpp.utils as utils


@attr.s(auto_attribs=True)
class SerialEchoHdlcTransport(SerialHdlcTransport):
    _skip: int = 0

    _serial: serial.Serial = attr.ib(
        default=attr.Factory(
            lambda self: serial.Serial(
                # parity is important to be even
                port=self.serial_port, baudrate=self.serial_baud_rate, timeout=5, parity=serial.PARITY_EVEN
            ),
            takes_self=True,
        )
    )

    def _write_bytes(self, to_write: bytes):
        debug(f"Sending: {utils.prettify(to_write)}")
        self._skip += len(to_write)
        self._serial.write(to_write)

    def _read_frame(self) -> bytes:
        if (self._skip > 0):
            skipped = self._serial.read(self._skip)
            debug(f'Skipped: {utils.prettify(skipped)}')
            self._skip -= len(skipped)

        in_bytes = self._serial.read_until(frames.HDLC_FLAG)

        if in_bytes == frames.HDLC_FLAG:
            # We found the first HDLC Frame Flag. We should read until the last one.
            in_bytes += self._serial.read_until(frames.HDLC_FLAG)

        if (len(in_bytes) > 0):
            debug(f'Received: {utils.prettify(in_bytes)}')

        return in_bytes
